package com.example.introduccionkotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {
    companion object {
        const val moneda = "USD"
    }

    var saldo : Float = 300.54f  ///Podemos especificar el tipo de variable con ": Float"
    var sueldo = 764.82f         ///Aunque tambien kotlin identifica que tipo de variable es solo especificando el valor
    var entero : Int = 30

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val fecha = "01/09/1992"
        //           0123456789 Para evaluar la subsecuencia linea 39
        var nombre ="Adrian"
        var vip:Boolean = false
        var inicial : Char = 'A'
        /*var saludo = "Hola " + nombre + "Su salario es: "+ saldo.toString()  /// es recomendable operar "Sumar, concatenar, etc" variables del mismo tipo, en este caso convertimos el saldo en "toString()"*/

        /*IF ELSE obligatorio los parentesis*/
        var saludo = "Hola " + nombre
        /*Si solo es una instruccion no es necesario llaves puede estar en la misma linea
        * if(vip == true) saludo += " Usted es VIP"
        * else saludo += " Puedes ser VIP si te suscribes"*/
        if(vip == true) {
            saludo += " Usted es VIP"
        }
        else{
            saludo += " Puedes ser VIP si te suscribes"
        }

        /*Para no utilizar if podemos utilizar
        when cuando ocurra lo que necesitamos evaluar
        es como un tipo de case*/
        var dia = fecha.subSequence(0,2).toString().toInt()
        if (dia == 1) ingresar_sueldo()

        var mes = fecha.subSequence(3,5).toString().toInt()
        when(mes){
            1,2,3 -> print("\n En invierno no hay ofertas de inversiones")
            4,5,6 -> print("\n En primavera hay ofertas de inversiones con el 1.5% de interés")
            7,8,9 -> print("\n En verano hay ofertas de inversiones con el 2.5% de interes")
            10,11,12 -> print("\n En Otoño hay ofertas de inversiones con el 3.5% de interes")
            else -> print("\n La fecha es erronea")
        }

        println(saludo)

        mostrar_saldo()

        /*Bucle do While se ejecuta hasta que se cumple el while
        * y bucle while solo se ejecutara mientras se cumpla la condicion*/
        var pin: Int = 1234
        var intentos = 0
        var clave_ingresada: Int = 1232

        /*Usar break*/
        do{
            println("Ingrese el PIN: ")
            println("Clave ingresada ${clave_ingresada++}")
            if (clave_ingresada == pin) break //Para salir del bucle o codigo de bloque
            intentos++
        }while(intentos < 3 && clave_ingresada != pin)

        if (intentos >= 3) println("Tarjeta bloqueada")

        ingresar_dinero(50.5f)
        retirar_dinero(40.87f)

        //region Operadores logicos
        /*Operadores logicos consultas mas complejas */
        /*var a: Boolean = true
        var b: Boolean = true
        var c: Boolean = true
        var d: Boolean = true
        val cc = 9;*/
        /*if (cc % 3 == 0) println("TRUE-")
        else println("False--")*/
        /*
        a && b  && -> "Y And"
        a || b  || -> "O Or"

        a && c
        a && c

        !d  -> Negacion
        */
        //endregion

        //region OperadoresDeCalculo

        /*OPERADORES DE CALCULO
        var a:Int = 5+5 //10
        var b:Int = 10-2 //8
        var c:Int = 3*4 //12
        var d:Int = 10+5 //2
        var e:Int = 10 % 3 //1 no se puede dividir, se queda con el resto
        var f:Int = 10 / 6 //1 division infinita, solo se mantiene la parte entera
        */
        var aPreIncremento: Int = 5
        var bPreDecremento: Int = 5
        var cPostIncremento: Int = 5
        var dPostDecremento: Int = 5

        println("PRUEBASS")

        /*incremento o decremento pre y post
        * Se puede hacer al inicio de la variable
        * Como al finalizar la variable */
        println(aPreIncremento)
        println(++aPreIncremento) //Incrementa primero, luego regresa. salida 6
        println(aPreIncremento)

        println(bPreDecremento)
        println(--bPreDecremento) //primero decrementa, luego regresa. salida 4
        println(bPreDecremento)

        println(cPostIncremento)
        println(cPostIncremento++) //primero regresa, luego decrementa. salida 5
        println(cPostIncremento)

        println(dPostDecremento)
        println(dPostDecremento--) //primero regresa, luego incrementa. salida 5
        println(dPostDecremento)

        /*
        Agregar comentarios por bloque
        con barra asterisco
        Asterisco barra
        */
        //saldo = saldo + sueldo
        /*saldo += sueldo //codigo mejorado recomendado por kotlin
        println(saldo)
        saldo++
        println(saldo)*/

        //endregion

        //region operadoresComparacion
        /*Operadores de comparacion retornan booleanos*/
        var EvaluaRespuesta : Boolean = false
        // a == b Si son iguales
        // a != b Si a es diferente a b
        // a > b Si a es mayor a b
        // a < b Si a es menor a b
        // a >= b Si a es mayor igual a b
        // a <= b Si a es menor o igual a b

        /*IF ELSE*/
        //endregion
    }

    /*Para declarar una funcion se utiliza "fun"*/
    fun mostrar_saldo(){
        println("Tienes $saldo $moneda")
    }

    fun ingresar_sueldo(){
        saldo += sueldo
        println("Se ha ingresado tu sueldo de $sueldo $moneda")
        mostrar_saldo()
    }

    /*para crear una funcion con parametro se declara el tipo de parametro*/
    fun ingresar_dinero(cantidad: Float){
        saldo += cantidad
        println("Se ha ingresado $cantidad $moneda a tu saldo")
        mostrar_saldo()
    }

    fun retirar_dinero(cantidad: Float){
        if(verificar_cantidad(cantidad)){
            saldo -= cantidad
            println("Se ha retiradp $cantidad $moneda a tu saldo")
            mostrar_saldo()
        }
        else{
            println("EL saldo es insuficiente para la cantidad: $cantidad $moneda")
        }
    }

    /*Funcion que retorna un booleano*/
    private fun verificar_cantidad(cantidad: Float): Boolean {
        return saldo >= cantidad
    }
}